"""
Test the loading and validation of config files
"""
import os
from pathlib import Path
import copy

from fastjsonschema import JsonSchemaValueException

script_dir = Path(__file__).absolute().parent
config_dir = Path(script_dir, 'data/config')
os.environ['FLOODTAGS_CONFIG_DIR'] = config_dir.as_posix()

from floodtags_sdk import config  # noqa:E402

# Use 'databases' part from global schema, to ensure the 'settings' property
# is there
global_schema = copy.deepcopy(config.global_schema)
databases_schema = global_schema['properties']['databases']
databases_schema['required'] = ['settings']

SCHEMA = {
    'type': 'object',
    'required': [
        'databases',
        'log_level'
    ],
    'additionalProperties': True,  # Since global.yaml contains more
    'properties': {
        'databases': databases_schema,
        'log_level': {
            'type': 'string',
            'enum': ['INFO', 'WARNING']
        },
    }
}


def test_config_loading():
    """
    Test if the loading of config files, and merging of config data works as
    expected
    """
    config_data = config.load_file(
        'specific.yaml', schema=SCHEMA, merge_global=True
    )

    # For 'databases', the subkeys should be merged, and for 'object_property',
    # the complete object should be replaced by the specific
    assert 'users' in config_data['databases']
    settings_url = config_data['databases']['settings']['url']
    assert settings_url == 'http://specific.specific'

    assert config_data['object_property'] == {'key3': 'specificvalue'}

    # When loading an invalid configuration (in this case: invalid log level)
    # the function should fail
    try:
        config_data = config.load_file(
            'specific-invalid.yaml', schema=SCHEMA, merge_global=True
        )
        raise TypeError('Loading should have failed!')
    except JsonSchemaValueException:
        pass
