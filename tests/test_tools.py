from floodtags_sdk import tools


def test_calculate_geohash():
    x = -4.329021
    y = 48.668983
    length = 9
    assert tools.calculate_geohash(x, y, length=length) == 'gbsuv7ztq'
