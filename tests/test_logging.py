"""
Test if the logs are generated as exepected in the correct location, and if
they can be parsed as JSON-lines
"""
import os
from pathlib import Path
import json

script_dir = Path(__file__).absolute().parent
output_dir = Path(script_dir, 'data/output/logs')
# Remove previous logs
if output_dir.is_dir():
    # remove files inside, and detele
    [p.unlink() for p in output_dir.iterdir()]
    output_dir.rmdir()
output_dir.mkdir(parents=True)
# Set as logging directory
os.environ['FLOODTAGS_LOG_DIR'] = output_dir.as_posix()


from floodtags_sdk import logging  # noqa:E402


def test_logging():
    # Initialize logger, and test logging
    logger = logging.get_root_logger(
        'floodtags-sdk', 'INFO', filename_suffix='-test'
    )
    logger.debug('Should not be logged')
    logger.info('Should be logged')

    logger.handlers[-1].flush()
    del logger.handlers[-1]  # prevent logging from other tests

    # Now check if the log content is there...
    log_path = Path(output_dir, 'floodtags-sdk-test.log.jsonl')
    log_records = []
    with open(log_path, 'r', encoding='utf8') as jsonlinesfile:
        for line in jsonlinesfile:
            log_records.append(json.loads(line))

    assert len(log_records) == 1
    assert log_records[0]['message'] == 'Should be logged'
    assert log_records[0]['source'] == 'floodtags-sdk'
