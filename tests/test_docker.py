"""
Test the docker module
"""
import os

from floodtags_sdk import docker


def test_docker_detection():
    """
    Docker should be detected when running in the gitlab CI, but not when
    running locally
    """
    if os.environ.get('CI') == 'true':
        assert docker.app_running_in_container()
    else:
        assert not docker.app_running_in_container()
