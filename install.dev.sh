# /bin/bash
set -e

# Remove possible old virtual env, for fresh installation
rm -r -f .env

# Create new virtual env and activate
python3.10 -m venv .env
source .env/bin/activate

# Install required packages
pip install --upgrade pip
pip install wheel
pip install pytest
pip install .

deactivate
