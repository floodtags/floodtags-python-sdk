# FloodTags SDK
This repository contains the 'floodtags_sdk' python package. This package
provides re-usable functionality that's used inside the FloodTags python
software components.

## 1 Installation
This package can be installed from the GitLab Package Registry for this
respository, using pip. The instructions to do this, can be found by clicking
the latest version of this package on the Package Registry page of the
GitLab group/project.

## 2 Functionality
The FloodTags SDK provides the following functionality:
* **Configuration file Loading**: Use `floodtags_sdk.config.load_file` to
  load configuration files from the FLOODTAGS_CONFIG_DIR, optionally applying
  global properties from global.yaml. To make sure meaningfull errors are
  triggered in case the file has a wrong format, configuration data is validated
  using JSON schema.
* **Docker Enhancements**: The `floodtags_sdk.docker.app_running_in_container()`
  function indicates if an application is running in Docker. If that is the
  case, use the `floodtags_sdk.docker.enable_stop_signal()` function to make
  sure the application is properly closed when `docker stop` is called.
* **Structured Loggging**: By initializing the root logger of the application
  using the `floodtags_sdk.logging.get_root_logger()` function, structured logs
  in JSON-lines format will be generated. These are automatically rotated and
  stored in the FLOODTAGS_LOG_DIR, so these can centralized using logging
  agents.

## 3 Development
To develop this package, the following is required:
1. Unix-based operating system (To ensure all scripts and githooks run)
2. Python 3.10

Prior to development, enable the githooks, to ensure tests are performed before
your commits:
```bash
./enable_githooks.sh
```

Install a local python environment for development by doing:
```bash
./install.dev.sh
```

Activate it after installation:
```bash
source activate.sh
```

If you extend the package, make sure to update the tests in the 'tests'
directory so these cover your changes. To run the tests, either make
sure your environment is up-to-date, and run the `pytest` command, or let the
`./test.sh` script take care of this for you.

When making changes, please note that users should be able to import modules
stand-alone without the need to configure variables for the other modules, or
other modules (other than io, tools and config) loading along with them.

Also the configuration variables required when loading a stand-alone module
should be limited. For example, when doing `from floodtags_sdk import config`,
only the FLOODTAGS_CONFIG_DIR should be required, and not the FLOODTAGS_LOG_DIR,
since the last one is only relevant for logging.py. Also users should be able
to leave out unused parts from sdk.yaml, if the relevant modules are not used,
so if parts are required by specific modules only, the schema used for loading
the file should not contain the 'required' key. Rather this check should
be performed on import, just like the logging.py and config.py modules check
the availability of relevant environment variables on import.

Configuration variables that depend on the layout of the local system or
container, such as directories, should always be loaded from a environment
variable, rather than a configuration file. This way, we can configure these
inside the docker-container itself, since multiple Docker containers, with
potentially different layouts, may use the same sdk.yaml through file binds.

## 4 Deployment
To be able to use a new version of the FloodTags SDK in other components, the
new package version must be uploaded to the FloodTags Python package registry:
1. Increment the version in [setup.py](setup.py)
2. Commit your changes
3. Tag the commit with the same version (e.g. `git tag 1.2.3`)
4. Push the commit and the tags: `git push && git push --tags`

This will trigger the build on GitLab, and upload the new version of the 
package to the package registry.
