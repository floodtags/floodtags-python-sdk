"""
Tools and functions for running FloodTags Applications in docker
"""
from pathlib import Path


def app_running_in_container() -> bool:
    """
    Returns if the current application is running inside a Docker container
    """
    if Path('/.dockerenv').is_file():
        return True

    cgroup_path = Path('/proc/self/cgroup')
    if cgroup_path.is_file():
        with open(cgroup_path, 'r', encoding='utf8') as cgroupfile:
            contents = cgroupfile.read()
            if '/docker/' in contents:
                return True

    return False


def _sigterm_handler(*args):
    """
    Handles the SIGTERM signal (issued by Docker stop)

    Raises a KeyboardInterrupt instead of calling sys.ext since some 3rd
    party modules have teardown code in place for KeyboardInterrupts
    """
    raise KeyboardInterrupt('SIGTERM (Docker Stop) signal received')


def enable_stop_signal():
    """
    Register a handler for the SIGTERM signal, so a KeyboardInterrupt is
    raised when docker stop is invoked.

    By default, 'docker stop' does nothing for Python applications, and they
    will be force closed after a pre-set interval (e.g. 10 seconds). After
    this function is invoked, a KeyboardInterrupt is raised, rather than
    sys.exit(), since some 3rd party modules have teardown code in place that's
    triggered by KeyboardInterrupts
    """
    import signal
    signal.signal(signal.SIGTERM, _sigterm_handler)
