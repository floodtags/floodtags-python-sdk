"""
Contains configuration loading functions, and loads the internal configuration
of the SDK on import
"""
from pathlib import Path

import fastjsonschema

import floodtags_schemas
from .tools import get_path_from_env_variable
from . import io

config_dir = get_path_from_env_variable(
    'FLOODTAGS_CONFIG_DIR', assert_dir=True,
)

global_schema = floodtags_schemas.config.global_
_validate_global_config = fastjsonschema.compile(global_schema)
_top_level_global_properties = set(global_schema['properties'].keys())


def load_file(
        filename: str, schema: dict, merge_global: bool = False
        ) -> dict | None:
    """
    Load the config file (YAML) with the given filename from the config
    directory (FLOODTAGS_CONFIG_DIR environment variable)

    Args:
        filename:
            Name of the configuration file to load

        schema:
            JSON schema dict to validate the final config against (Note: If
            you set merge_global=True, you should also allow
            additionalProperties at top-level in your schema, and set the ones
            that you depend on to 'required'. You can deepcopy the
            .global_schema attribute of this module to re-use those parts)

        merge_global:
            If true, global.yaml is loaded first (when exists). Any properties
            defined in the specific file, will override the ones from
            global.yaml

    Returns:
        A dict in case configuration data can be loaded (either from the
        global file, the specific one, or both) and None in case none of the
        files is present
    """
    config = {}
    global_config_path = Path(config_dir, 'global.yaml')
    mergeable_properties = set()
    if merge_global and global_config_path.is_file():
        config = _validate_global_config(
            io.load_yaml_file(global_config_path)
        )
        # Just using config.keys() is not the way to go, since this can contain
        # additional properties that are not validated by the global schema
        mergeable_properties = _top_level_global_properties.intersection(
            config.keys()
        )

    specific_config_path = Path(config_dir, filename)
    if specific_config_path.is_file():
        specific_config_data = io.load_yaml_file(specific_config_path)
        merge_values_of_keys = mergeable_properties.intersection(
            specific_config_data.keys()
        )
        if merge_values_of_keys:
            # Merge sub-properties for these
            for key in merge_values_of_keys:
                config[key].update(specific_config_data.pop(key))

        config.update(specific_config_data)
    elif not merge_global or not global_config_path.is_file():
        return None

    return fastjsonschema.validate(schema, config)
