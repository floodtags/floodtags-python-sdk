"""
Contains the logic to handle connections to the message broker (RabbitMQ)
"""
from queue import Queue, Empty
import logging
import threading
import time
import signal
import json
import os
from typing import Any
import ssl

import pika
from pika.exceptions import AMQPError

from .io import save_jsonlines_file
from .tools import get_path_from_env_variable

logger = logging.getLogger(__name__)

publish_kwargs = {
    'ingestion': {
        'exchange': 'ingestion',
        'routing_key': '',
    },
}


class Publisher:
    """
    Class to handle publishing messages to the broker

    Note that after initialization this starts a seperate deamon thread for
    sending data, so heartbeats are handled correctly, and the connection is
    kept online.

    Use the .publish() method to send messages to the message broker
    """
    def __init__(
                self, *, host: str, port: int, username: str, password: str,
                component_id: str, component_version: str,
                vhost: str = 'pipeline', socket_timeout: int = 2,
                stack_timeout: int = 3, local_file_path: os.PathLike = None,
                confirm_delivery: bool = False, retries: int = 2,
                heartbeat_interval: int = 10,
                ):
        """
        Initializes a Publisher instance

        Args:
            host:
                IP address or FQDN of RabbitMQ host
            port:
                AMQP port number
            username:
                Username for basic auth
            password:
                Password for basic auth
            component_id:
                Id of the component used for the connection name (e.g.
                'ingestion-meltwater')
            component_version:
                Version used in the connection name (e.g. '1.0.0')
            vhost:
                Name of virtual host to select on instance
            socket_timeout:
                Timeout value for connection socket
            stack_timeout:
                Timeout value for complete request
            local_file_path:
                If provided, messages are written here on failure
            confirm_delivery:
                If True, confirm_delivery is enabled for the channel, and
                the 'mandatory' flag is set on each message to ensure it's
                queued at least once
            retries:
                The number of times to retry sending data to the queue on
                failure
            heartbeat_interval:
                The number of seconds in between sending heartbeats. Heartbeats
                are send at intervals between this value and twice this value.
        """
        ca_cert_loc = get_path_from_env_variable(
            'FLOODTAGS_INTERNAL_CA', assert_file=True,
        )
        ssl_context = ssl.create_default_context(
            cafile=ca_cert_loc,
        )
        self._connection_options = {
            'host': host,
            'port': port,
            'vhost': vhost,
            'username': username,
            'password': password,
            'socket_timeout': socket_timeout,
            'stack_timeout': stack_timeout,
            'ssl_options': pika.SSLOptions(ssl_context, host)
        }
        self.local_file_path = local_file_path
        self.confirm_delivery = confirm_delivery
        self.connection_name = f'{component_id}@{component_version}'
        self.retries = retries
        self.heartbeat_interval = heartbeat_interval
        self._connection = None
        self._channel = None
        self._queue = Queue()
        self._publish_thread = threading.Thread(
            target=self._run, daemon=True,
        )
        self._publish_thread.start()

    @property
    def connection(self) -> pika.BlockingConnection:
        """
        The pika.BlockingConnection used to connect to the broker
        """
        if self._connection is None or self._connection.is_closed:
            self._connection = pika.BlockingConnection(
                pika.ConnectionParameters(
                    host=self._connection_options['host'],
                    port=self._connection_options['port'],
                    virtual_host=self._connection_options['vhost'],
                    credentials=pika.PlainCredentials(
                        self._connection_options['username'],
                        self._connection_options['password']
                    ),
                    socket_timeout=self._connection_options['socket_timeout'],
                    stack_timeout=self._connection_options['stack_timeout'],
                    ssl_options=self._connection_options['ssl_options'],
                    client_properties={
                        'connection_name': self.connection_name,
                    }
                ),
            )
        return self._connection

    @property
    def channel(self) -> pika.adapters.blocking_connection.BlockingChannel:
        """
        The pika.adapters.blocking_connection.BlockingChannel for publishing
        """
        if (
                self._channel is None or self._channel.is_closed or
                self._connection.is_closed
                ):
            self._channel = self.connection.channel()
            if self.confirm_delivery:
                self._channel.confirm_delivery()
        return self._channel

    def _send_heartbeat(self):
        """
        Send a heartbeat to keep the connection open
        """
        try:
            self.connection.process_data_events()
        except (AMQPError, ValueError):
            logger.warning('AMQP Heartbeat failed', exc_info=True)

    def _publish(self, message, **kwargs):
        """
        Triggers the basic_publish method of the channel, retries on failure

        Args: (see publish() method)
        """
        kwargs['body'] = serialize_message(message)
        for i in range(self.retries):
            try:
                self.channel.basic_publish(**kwargs)
                break
            except (AMQPError, ValueError):
                logger.warning(
                    'Message publish failed. Retrying...', exc_info=True,
                )
                time.sleep(0.5)
        else:
            try:
                self.channel.basic_publish(**kwargs)
            except (AMQPError, ValueError):
                log_message = 'Message publish failed.'
                if self.local_file_path is not None:
                    log_message += ' Saving data locally...'
                logger.exception(
                    log_message
                )
                if self.local_file_path is not None:
                    save_jsonlines_file(
                        [message], self.local_file_path, mode='a'
                    )

    def _run(self):
        """
        Listens to the queue for new data to send

        This method will be run continuously in the _publish_thread
        """
        self.channel  # Just to initialize the connection
        last_heartbeat = time.time()
        while True:
            try:
                message, kwargs = self._queue.get(
                    block=True, timeout=self.heartbeat_interval
                )
                self._publish(message, **kwargs)
                if time.time() - last_heartbeat > self.heartbeat_interval:
                    self._send_heartbeat()
                    last_heartbeat = time.time()
            except Empty:
                self._send_heartbeat()
                last_heartbeat = time.time()
            except Exception:
                # A Catch all to stop the application if there is an unexpected
                # fail
                logger.critical(
                    (
                        'Unhandled exception in data publishing thread'
                        ', stopping application...'
                    ),
                    exc_info=True
                )
                # Since calling sys.exit only stops the thread, we need to
                # trigger a KeyboardInterrupt for main process
                signal.raise_signal(signal.SIGINT)

    def publish(self, message=dict, **kwargs):
        """
        Like pika.channel.basic_publish, but ensuring data is published via
        a seperate thread

        Args:
            message:
                The data to send. Must be JSON-serializable. Will be
                JSON serialized and utf-8 encoded and passed as 'body' to
                pika.channel.basic_publish
            **kwargs:
                Other pika.channel.basic_publish kwargs
        """
        if not self._publish_thread.is_alive():
            raise RuntimeError(
                'AMQP Connection thread crashed'
            )

        if not kwargs:
            raise TypeError(
                'Please provide keyword arguments for the publish function'
            )

        if self.confirm_delivery:
            kwargs['mandatory'] = True

        self._queue.put_nowait((message, kwargs))


def serialize_message(message: Any) -> bytes:
    """
    Returns the UTF encoded JSON serialized message
    """
    str_ = json.dumps(message, ensure_ascii=False)
    return str_.encode('utf-8')


def deserialize_message(raw_message: bytes) -> Any:
    """
    Deserialize a UTF encoded JSON payload
    """
    str_ = raw_message.decode('utf-8')
    return json.loads(str_)
