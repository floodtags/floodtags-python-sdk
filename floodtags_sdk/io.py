"""
filesystem input/output functions
"""
from pathlib import Path
from typing import Any
import json

import yaml


def load_yaml_file(path: Path) -> Any:
    """
    Loads the data from the YAML file at the given path

    Args:
        path: Location of the YAML file

    Returns:
        Parsed YAML file contents
    """
    with open(path, 'r', encoding='utf8') as yamlfile:
        return yaml.safe_load(yamlfile)


def save_jsonlines_file(data: list[Any], path: Path, mode: str = 'w'):
    """
    Saves the data to a JSON-lines file

    Args:
        data: list of JSON-serializable items
        path: Location of the file
        mode: Write mode to use
    """
    with open(path, mode, encoding='utf8') as jsonlinesfile:
        for item in data:
            jsonlinesfile.write(
                json.dumps(item, ensure_ascii=False) + '\n'
            )
