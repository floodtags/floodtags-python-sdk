"""
General functions for the FloodTags SDK package
"""
from pathlib import Path
import os


def get_env_variable(variable_name: str) -> str:
    """
    Gets the value of the given env variable. Raises a well explained KeyError
    if it doesn't exist
    """
    try:
        return os.environ[variable_name]
    except KeyError:
        raise KeyError(
            f'Environment variable "{variable_name}" is not defined!'
        ) from None


def get_path_from_env_variable(
        variable_name: str, assert_dir: bool = False,
        assert_file: bool = False
        ) -> Path:
    """
    Returns the path stored in the given environment variable

    Args:
        variable_name: Name of the environment variable
        assert_dir: If 'True', raise a ValueError if it's not a valid directory
        assert_file: If 'True', raise a ValueError if it's not a valid file

    Returns:
        The path stored in the environment variable

    Raises:
        KeyError: If the environment variable is not set
        ValueError: If assert_dir or assert_file is provided
    """
    contents = get_env_variable(variable_name)
    path = Path(contents)
    if assert_dir and not path.is_dir():
        raise ValueError(
            f'Directory "{contents}" from env var "{variable_name}" does not exist!'  # noqa: E501
        )
    if assert_file and not path.is_file():
        raise ValueError(
            f'File "{contents}" from env var "{variable_name}" does not exist!'  # noqa: E501
        )
    return path


def calculate_geohash(
        x, y, length=9,
        ) -> str:
    """
    Calculate the geohash of the input latitude/longitude
    https://en.wikipedia.org/wiki/Geohash#Algorithm_and_example
    """
    b32 = '0123456789bcdefghjkmnpqrstuvwxyz'
    x_window = [-180, 180]
    y_window = [-90, 90]

    iterations = 5 * length
    geohash = ''

    bits = ''
    for i in range(iterations):
        if i % 2 == 0:
            x_middle = sum(x_window) / 2
            if x < x_middle:
                bits += '0'
                x_window[1] = x_middle
            else:
                bits += '1'
                x_window[0] = x_middle
        else:
            y_middle = sum(y_window) / 2
            if y < y_middle:
                bits += '0'
                y_window[1] = y_middle
            else:
                bits += '1'
                y_window[0] = y_middle

        if (i + 1) % 5 == 0:
            ind_ = int(bits, base=2)
            letter = b32[ind_]
            geohash += letter
            bits = ''

    return geohash
