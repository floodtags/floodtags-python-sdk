"""
Contains the FloodTags specific configuration for logging. Any operational
FloodTags software component should use the 'get_root_logger' function to
initialize the root logger, to ensure logs can be properly centralized and
monitored.
"""
from pathlib import Path
import logging
from logging import handlers
import datetime
import json
import sys

from .tools import get_path_from_env_variable


class Formatter(logging.Formatter):
    """
    Formatter for creating JSON-lines log files

    Attributes:
        source_name: The name if the component doing the logging
    """
    def __init__(self, *args, source_name: str, **kwargs):
        self.source_name = source_name
        super().__init__(*args, **kwargs)

    def get_error_info(self, record: logging.LogRecord) -> str | None:
        """
        Gets an error information string in case an error occured. Adapted from
        builtin logging.Formatter
        """
        s = ' '
        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            # (it's constant anyway)
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + record.exc_text
        if record.stack_info:
            if s[-1:] != "\n":
                s = s + "\n"
            s = s + self.formatStack(record.stack_info)

        if s == ' ':
            return None
        else:
            return s.strip()

    def format(self, record: logging.LogRecord) -> str:
        """
        Formats log records as a JSON serialized string
        """
        err = record.exc_info
        if err is False or err is True:
            # This is somehow the case for some elasticsearch package errors..
            error_type = None
        elif err is not None:
            error_type = record.exc_info[0].__name__
        else:
            error_type = None

        payload = {
            'level': record.levelname,
            'date': datetime.datetime.utcfromtimestamp(
                        record.created
                    ).isoformat(),
            'message': record.getMessage(),
            'source': self.source_name,
            'subModule': record.name,
            'errorType': error_type,
            'errorInfo': self.get_error_info(record)
        }

        return json.dumps(payload, ensure_ascii=False)


def get_root_logger(
        component_name: str, level: str | int, filename_suffix: str = ''
        ) -> logging.Logger:
    """
    Get a root level logger, that logs all information using a rotating file
    handler to the FLOODTAGS_LOG_DIR

    Args:
        component_name:
            The name of the component that is logging, used in the filename of
            the logs, and in the 'source' property of log records

        level:
            The level passed to the logging library (see
            https://docs.python.org/3/library/logging.html#logging-levels)

        filename_suffix:
            This will be appended to the filename (before the extension), but
            not to the 'source' field. Usefull when logging from multiple
            processes.

    Returns:
        The configured root logger
    """
    # Get root logger
    logger = logging.getLogger()

    # Determine file location, and set rotating file handler
    logs_dir = get_path_from_env_variable('FLOODTAGS_LOG_DIR', assert_dir=True)
    file_loc = Path(logs_dir, f'{component_name}{filename_suffix}.log.jsonl')
    handler = handlers.RotatingFileHandler(
        file_loc, maxBytes=1048576, backupCount=4,
    )

    formatter = Formatter(source_name=component_name)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logger.setLevel(level)
    handler.setLevel(level)

    # Replace the default exception printing function, so that unhandled
    # exceptions are logged before stopping the script
    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            logger.warning(
                "STOPPED because of signal",
                exc_info=(exc_type, exc_value, exc_traceback)
            )
        else:
            logger.critical(
                "A critical error occured",
                exc_info=(exc_type, exc_value, exc_traceback)
            )
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    sys.excepthook = handle_exception

    return logger
