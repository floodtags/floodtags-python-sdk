# -*- coding: utf-8 -*-
from setuptools import setup

setup(
    name="floodtags-sdk",
    version="3.1.7",
    packages=[
        'floodtags_sdk',
    ],
    install_requires=[
        'pyyaml>=6.0.1,<7.0',
        'fastjsonschema>=2.14.3,<3.0.0',
        'floodtags-schemas>=2.0.0,<3.0.0',
        'pika>=1.3.2,<2.0.0'
    ],
    python_requires='>3.10.0',
    author="FloodTags",
    author_email="info@floodtags.com",
    description='Provides common functions and configuration used by FloodTags python components'
)
