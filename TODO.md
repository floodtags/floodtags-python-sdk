## TODOs
After initial v2.0 release, with the config, docker and logging modules, the
following functionality from sdk v1.x.x needs to be ported (See
maintenance/1.x.x branch):
- [ ] Implement sources.py module for loading the sources data
- [ ] Implement adapter.sync and adapter.async modules for sync and async
      adapter communication
- [ ] Implement formats.py for format validation; Make use of subclassed
      (User)dicts with JSON-schema validation. Add the 'timezones' list to 
      a JSON-schema.
- [ ] Implement media.py, which determines filenames of content (By id) and
      saves them by date
- [ ] Implement broker.py, which contains the functionality currently in the
      messagequeues submodule
